# React

Librería de código abierto, desarrollada en JavaScript, para crear interfaces de usuario.
React es declarativo y reactivo gracias al state de los componentes. **Declarativo** porque no le indicamos *como* debe reenderizar sino *que* debe renderizar y **Reactivo** porque reacciona a los cambios de los componentes.
Ofrece un **flujo de datos unidireccional**, los datos fluyen en única dirección y es de padre a hijo, esto significa que un componente pasa su estado "hacía abajo" y cada vez que es actualizado provoca que todos sus componentes hijos se vuelvan a renderizar.


## Características
* Declarativo y basado en componentes.
* Indicamos el QUÉ no el CÓMO.
* Esta basado en componentes que dividen la interfaz en trozos pequeños y reutilizables de código.
* Cada componente encapsula su estado.
* La aplicación es renderizada en el servidor, perfecto para SEO y performance


### Programación reactiva
Cada componente reacciona y se vuelve a renderizar cuando se produce un cambio de estado o le llegan nuevas propiedades.

### Virtual DOM y diffing
Genera una copia del árbol de elementos del navegador para solo hacer los mínimos cambios necesarios para reflejar el estado de nuestros componentes.

### Eventos sintéticos
* Abstacción de los eventos nativos de los navegadores.
* Compatibilidad croos browsing sin necesidad de más librerías. 


## Componentes React
No dejan de ser funciones JavaScript que aceptan párametros llamados props o propiedades y que devuelven elementos de React que describen lo que debe aparecer en pantalla.
Todos los componentes tienen que actuar como funciones puras y por lo tanto, no pueden modificar sus propiedades

*CLASSIC FUNCTION*

      function Hello (props) {
        return <h2>{ props.title }</h2>
      }

*AROUND FUNCTION*
    
      const Hello = (props) => <h2>{props.title}</h2>

*CLASS*
    
      class Hello extends Component {
        render () {
          return <h2>{this.props.title}</h2>
        }
      }


## JSX
Sintaxis para desarrollar componentes en React. Ejemplo:

JSX | Transformación 
--- | --------------
`const element = <h1>Hello, world</h1>` | `var element = React.createElement("h1", null, "Hello, world");`


## props
Son los parametros de los componentes.
Los componentes deben ser usados como funciones puras donde los parametros son las props, es decir, los valores de las props son inmutables.
Las props son de solo lectura, React no permite su modificación.
Las props cambian su renderizado, lo cual es muy útil para cambiar el comportamiento de un componente


## state
Para hacer reactivos a los componentes se utilza el State.
React, funciona de forma que optimiza, cuando debe hacer los cambios en el árbol de elementos del navegador, para ello tiene una cola que va priorizando el trabajo que debe realizar para reflejar los cambios de nuestros componentes. **Para actualizar el estado de nuestros componentes** debemos utilizar el método *setState*, que se encarga de hacer una petición a la cola para saber cuando es el momento mas adecuado para actualizar nuestro componente. El método es asíncrono y acepta un párametro que es el nuevo estado que queremos en nuestro componente.
Del State sabemos que:
* Debe tratarse como información innmutable.
* Solo se puede actualizar con el método setState.
* Es asíncrono.
* **Para añadir el state al componente** tenemos que utilizar el constructor de la clase y llamar al método *super()* que ejecturá el método constructor de la clase que extendemos (En la mayoría de los casos, Component). Para inicializar el valor del state se utiliza *this*, que es el contexto del componente, se apunta al state, y se pasa un objeto con el estado incial del componente.
```
class Ejemplo extends Component {
  constructor() {
    super()
    this.state = { var: 1 }
  }
}
```
* **Cada que hay un cambio de state nuestro componente y sus componentes hijos son renderizados.**

## Enlazar el contexto correcto
Existen dos métodos:
**Método .bind()**. Esta disponible como propiedad en todas las funciones de JavaScript. Devuelve el mismo método pero con el contexto que se ha pasado como parámetro enlazado a la ejecución de la funcíon (contexto correcto).
**Around Function**. 


## Ciclo de vida de los componentes
Se divide en tres fases:
* **Montaje**. Se ejecuta siempre y solo lo hace una vez, es la encargada de construir los componentes con su estado inicial y obtiene las props que se le han pasado. Se ejecuta por primera vez el método render.
Ejecuta los métodos *constructor(props)*, *componentWillMount*, *render()* y *componentDidMount* en ese orden.
* **Actualización**. Se ejecuta cada vez que nuestro componente recibe nuevas props o su estado se ha actualizado. Permite controlar cuando el componente necesita renderizarse de nuevo para reflejar los cambios.
* **Desmontaje**. Sirve para eliminar cualquier listener que hayamos creado y que disponga de metodos que hagan referencia a algunos de los componentes que puedan no estar disponibles en el DOM.

### Montaje
Se ejucuta siempre y solo lo hace una vez, al iniciar nuestro componente. En este ciclo se crea el componente, se incializa el state y ocurre el primer render. Al finalizar este ciclo ya tendrémos disponible una primer representación de nuestro componente en el árbol de elementos.
#### Método constructor(props)
Existe en todas las clases y sirve para crear e inicializar la clase. Los componentes de React también cuentan con este método
Es el primero en ser ejecutado, siempre se ejecuta y es donde se incializa el state de nuestro componente.
No se debe llamar al método *setState* ya que el state no puede ser actualizado.
#### Método componentWillMount()
Se ejecuta una sola vez justo antes del método render() y es el primer método en ejecutarse. Aún no se tiene el componente disponible en el DOM. Prepara la configuración incial del componente, incializa el state y tiene listo el código y todos los datos que se necesitan para el primer renderizado. Se recomienda usar el constructor en su lugar. Se puede usar el setState y no provoca otro render.
#### Método render()
Es el único método obligatorio para que los componentes puedan funcionar. Se encarga de una única cosa que es dibujar, en pantalla, la representación que deseamos de nuestro componente. Esta representación puede estar hecha con elementos del DOM (`<h1>, <p>,` etc), puede devolver un array de elementos, o incluso un valor null que indica que no debe renderizar nada.
Debe ser un método siempre puro, es decir, no debe modificar el state (llamar setState) y no interactuar con el browser, ya que provocaría un loop infinito.
Evitar operaciones y transformaciones en este método.
#### Método componentDidMount()
Se ejecuta tras renderizar el componente, ya que se tiene una representación en el DOM. Es el método perfecto para realizar llamadas a servicios externos, añadir llamadas para reuperar datos del servidor y escuchar eventos del navegador, por ejemplo hacer scroll en la página.

### Actualización
Permite que React sea reactivo y declarativo, es decir, reacciona a los cambios que se producen en el state de un componente cada vez que le llegan props nuevas. Le decimos **que** debe renderizar y no **como** renderizarlo. Este ciclo esta continuamente en ejecución, desde que se monta el componente hasta que se desmonta. El ciclo también permite indicar si el renderizado es necesario o no.
#### Método componentWillReceivedProps(nextProps)
Solo se ejecuta si el ciclo de actualización ha sido provocado porque el componente a recibido nuevas props, y no cuando su state ha cambiado. En este método se reciben como parámetro las nuevas props, de forma que se pueden usar para, por ejemplo, actualizar el state del componente incluso antes de reflejar las props que llegan en el método render.
#### Método shouldComponentUpdate(nextProps, nextState)
Determina si el componente se debe actualizar.
Se ejecuta después del componentWillReceivedProps() y antes de actualizar el compenente. 
Se ejecuta en caso de que el ciclo de actualización haya sido provocado por un cambio de state en el propio componente. Decide si el componente vuelve a ejecutar su método render, enviando un *true* para seguir con el ciclo de actualización, rederizando así el componente.
Si no se escribe un método shouldComponentUpdate() para el componente, significa que por defecto React siempre renderizará el componente cuando detecte un cambio en el state o en las props.
Si el método devuelve un *false* el ciclo de actualización términa.
#### Método componentWillUpdate(nextProps, nextState)
Se ejecuta justo antes del método render. Su utilidad se limita a cosas que vamos a querer preparar en el árbol de elementos para antes de renderizar los cambios, por ejemplo, realizar algún tipo de animación de los elementos en el DOM. Pero estas son cosas que podemos conseguir a base de cambiar las bases CSS que usamos en nuestros elementos en el render.
Es la última oportunidad de ejecutar código antes de que el método render se lance.
No se debe utilizar el setState o entrará en un loop infinito.
#### Método render()
#### Método componentDidUpdate(prevProps, prevState)
Se ejecuta una vez que se concluyó el método render(). En este punto se tiene el html resultante del render en el DOM del navegador, por lo que normalmente este método se utiliza para consultar directamente estos elementos actualizados o enviar un evento untraking para indicar que ha cargado un nuevo componente que puede ser una página o para hacer una llamada a una API justo después del renderizado, esto último no es muy frecuente, ya que normalmente se realizan llamadas a APIs como consecuencias de eventos más que a consecuencia de un renderizado.  

### Desmontaje
Se ocupa de desmontar nuestro componente de la vista de la aplicación cuando deja de ser necesario. 
Se puede ejecutar una vez en la vida del componente y ofrece un método como punto de captura *componentWillUnmount()*
#### Método componentWillUnmount()
Se ejecuta justo antes de desmontar completamente nuestro componente, nos permite eliminar suscripciones de eventos del DOM, cancelar peticiones externas a la red, limpiar intervalos o liberar recursos que estemos utilizando y no vamos a usar mas.

### Ciclo de error
Introducido en la versión 16 de React. Permite recuperar nuestra aplicación de errores que se producen y que no son controlados. En esta fase solo se ejecuta el método *componentDidCatch()*
#### Método componentDidCatch(error, info)
Solo se ejecuta si se tiene un error o excepción. Se debe tener en cuenta que el método captura los posibles errores y excepciones de los componentes *children*, es decir, los componentes que estemos utilizando en nuestro componente, y no el componente en si mismo, para ello siempre se deberá llevar este método a un nivel superior.
No captura los errores:
* que se producen en los métodos disparados por eventos, por ejemplo, una función que maneje el click de un botón, 
* en el código asíncrono de nuestra aplicación, en el renderizado que se puede hacer en el lado del servidor.
Si un componente tiene un error NO controlado, se desmontará totalmente el componente de nuestra aplicación.


## Fetch API
Interfaz en JavaScript para hacer peticiones a recursos remotos a través de la red, es compatible con todos los navegadores. Gestiona la respuesta en forma de promesas y funciona con todo tipo de recursos, el uso mas típico es el pedir información a una Application Programming Interface (API) con una serie de párametros para que nos devuelva la respuesta en JSON para luego mostrar los datos en pantalla.


## Buenas Pácticas
**Si los componentes son clases ¿Por qué no los extendemos cuando tengamos que modificarlos utilizando la herencia?**
Funciona pero no es muy escalable, el código es menos declarativo, mas engorroso y además conforme vaya creciendo no sabremos las ramificaciones que tendrán nuestros componentes
En lugar de implementar la *herencia* se usa la *composición*, es decir componer componentes de React utilizando otros.
En React la composición es la forma correcta de crear variaciones de los componentes y siempre se debe evitar la herencia de clases.
**Stateless components (Componentes Funcionales Puros)** 
Componentes que no tienen y ningún tipo de *state* y también carecen del uso de algún *método del ciclo de vida*. 
Es mejor que estos componentes sean funciones (**Componentes Stateless**) en lugar de clases (**Componentes Stateful**).

### Patrón Contenedor-Contenido
Patrón de diseño para la creación de componentes. Se trata de dividir nuestros componentes en dos categorías diferenciadas: Componentes *Contennodes* y *Contenidos* (también conocidos como Componentes Listos y Tontos o Componentes Lógicos y Presentacionales, respectivamente).
#### Componentes Contenedor
Se encargan de manejar la lógica del componente, teniendo un state interno donde guardará la información recuperada de recursos externos, como una API, y haciendo todas las transfarmaciones necesarias para adecuar los datos recuperados a las props que se pasarán a los Componenentes Contenido.
No deben tener nunca en el `render()` nada que tenga que ver con el Layout de la aplicación, por ejemplo tags `<div>`.
#### Componenets Contenido
Su único objetivo es: Utilizar las props que recibe para renderizar en pantalla el layout mostrando los elementos que representen estos datos.
No se puede tener ninguna llamada a ningún recurso externo, como al server o alguna API. Debemos pensar que son como componentes puros que siempre que le pasemos las mismas props su representación en pantalla será la misma.

### Component StrictMode
Disponible desde la version 16.3. 
Para saber si nuestra aplicación utiliza funcionalidades obsoletas, para seguir las mejores prácticas de React y para poder migrar a nuevas versiones de la librería podemos utilizar el component *StricMode*. 
Para usarlo tenemos que ir al punto de entrada de la aplicación, donde renderizamos el componente raíz, ahí importamos el componente (`import React, { StricrMode } from 'react';`) y envolvemos el componente `<App />` con este componente `<StrictMode>` (`<StrictMode>...<App />...</StrictMode>`) y en la consola de la herramienta de desarrollo comprobamos si tenemos alguna advertencia. Nos puede avisar que estamos utilizando métodos obsoletos, o que estamos generando efectos secundarios inesperados por usar el state incorrectamente.
Tener en cuenta que este componente no renderiza nada visual y solo funciona en modo desarrollo.



# Redux

Controlar el estado cambiante de una aplicación es difícil, en cierto punto ya no se entiende que esta pasando ya que se llega a perder control sobre el cuando, el porque y el como de su estado.
**Redux** es un sistema determinista, permite que los cambios en el estado global y sus consecuencias puedan ser predecibles y fácilmente replicables.

* Redux es una librería externa que no pertenece a React.


## Conceptos básicos.
El estado de tu aplicación:
* se describe como un simple objeto.
* es global y puedes recuperarlo desde cualquier parte de la vista.
* no se puede modificar, se debe generar uno nuevo a partir del anterior a través de acciones.


## Principios.
* *1. Solo se tiene una única fuente de la verdad.* Todo el estado de la aplicación estará contenido en un único **store**
* *2. El estado es de sólo lectura (inmutable).* No se puede modificar libremente y tampoco puede ser cambiado por un efecto secundario, solo se puede modificar virtualmente creando un estado nuevo mediante un **action** a partir del estado anterior.
* *3. Los cambios se realizan con funciones puras.* Los nuevos estados se generan utilizando funciones puras que siguen dos reglas básicas: 1. Dados los parámetros de entrada de identico valor, la función siempre devolvera el mismo resultado y 2. El computo de la función (su lógica) no implican ningún efecto observable colateral fuera de ella. A estas funciones en redux se les llaman **reducers**.

### Actions
Son objetos planos de JavaScript, deben tener de forma obligarotia una key llamada "type" que indica el tipo de acción que se quiere hacer, el valor es un String en mayúsculas, además del type pueden tener más propiedades que funcionan como *payload* para realizar la acción.

**Action Creators** Funciones puras que al ejecutarlas devuelven un Action en forma de objeto, evitando el tener que escribir las acciones de manera constante.

### Reducers
Funciones puras que reciben dos parámetros: 
1. El estado anterior de nuestra aplicación y 
2. La action que lo esta llamando 

Utilizando ambos parámetros generará el próximo estado global de nuestra aplicación, entonces se le puede llamar *"generador de estados"* 

### Store
Lugar que almacena el estado global de la aplicación. Ofrece métodos para recuperar el estado cuando queramos, suscribirnos a la generación de un nuevo estado para reaccionar automáticamente y permite enviarle acciones para generar estados nuevos.




# React - Redux 

La librería reac-redux (npm i -s react-redux) nos facilita conectar nuestros componentes de React a la store de Redux. También nos permite acceder facilemente a las partes de estado global que nos interensen y recibir de forma automática todas las actualizaciones que se realicen a ellas. Además permite despachar acciones a cualquiera de nuestros componentes.

## Patrón Contenedor-Contenido
* Los componentes representacionales (Component o Contenido) no deberían saber nada de Redux, debería siempre leer los datos de las props, pueden tener state propio o interno pero no deben acceder nunca a la store global, además para manipular los datos del estado global deben acceder siempre a las props, nunca desde una acción (action).

* Los componentes contonederos tratan con Redux, se suscriben a la parte de la store que les interesa y envían directamente las actions a Redux para que actualice el state global.


## API react-redux 
Proporciona dos utilidades, un método **connect()** y un componente **`<Provider />`** 

### Método connect().
Permite conectar los componentes de React a la store de Redux; al ejecutarse nos devuelve una función a la que se le tiene que pasar el componente que queremos conectar a la store de Redux. Para ello se deben pasar dos parámetros:
* **mapStateToProps** función que puede acceder al stat global y extrae lo que necesitamos, en esta función se devuelve un objeto y cada key del objeto será una prop para el componente conectado, cada vez que el valor de la key se actualice lo enviará al prop del componente generando un nuevo ciclo de vida.
* **mapDispatchToProps** función que recibe como parámetro la función dispatch que podemos utilizar para envíar acciones a la store y actualizar su estado, en esta función también se devuelve un objeto y cada key del objeto será una prop para el componente conectado y cada prop llamará a su acción correspondiente para actualizar el state global según las configuraciones de los reducers.

### Componente <Provider />.
Nos permite tener la store disponible en todos nuestros componentes. Donde queremos implementarlo basta con rodear el componente con sus tags y enviar como prop la store que hayamos creado (`store={store}`)
